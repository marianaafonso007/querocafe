﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QueroCafe.Filters;
using QueroCafe.Models;
using QueroCafe.Models.Entity;
using QueroCafe.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Controllers
{
    [Autenticacao]
    public class CheckinController : Controller
    {
        private readonly AgendaRepository agendaRepository;
        private readonly CheckinRepository checkinRepository;
        private readonly CafeRepository cafeRepository;
        private readonly CafeHistoricoRepository cafeHistoricoRepository;

        public CheckinController(IConfiguration configuration)
        {
            this.checkinRepository = new CheckinRepository(configuration);
            this.agendaRepository = new AgendaRepository(configuration);
            this.cafeRepository = new CafeRepository(configuration);
            this.cafeHistoricoRepository = new CafeHistoricoRepository(configuration);
        }

        public IActionResult Index()
        {
            var checkin = new Checkin();
            var agenda = agendaRepository.Buscar(Sessao.IdUsuario, DateTime.Now);
            var cafe = cafeRepository.BuscarPorStatus(CafeStatus.Comprado);

            if (agenda != null)
            {
                checkin = checkinRepository.BuscarPorAgenda(agenda.ID);

                if (checkin == null)
                    checkin = new Checkin() { Agenda = agenda };
            }

            ViewBag.Cafe = cafe;

            return View(checkin);
        }

        public IActionResult Marcar()
        {
            var agenda = agendaRepository.Buscar(Sessao.IdUsuario, DateTime.Now);
            var checkin = new Checkin() { Agenda = agenda };

            checkinRepository.Incluir(checkin);

            if (agenda.Acao == TipoAcao.Comprar)
            {
                cafeRepository.Incluir(new Cafe()
                {
                    Agenda = agenda,
                    QtdXicaras = 5,
                    Status = CafeStatus.Comprado
                });

                cafeHistoricoRepository.Incluir(new CafeHistorico()
                {
                    CafeID = cafeRepository.BuscarPorStatus(CafeStatus.Comprado).ID,
                    UsuarioID = Sessao.IdUsuario,
                    Status = CafeStatus.Retirado
                });
            }

            ViewBag.Sucesso = true;

            return View("Index", checkin);
        }
    }
}
