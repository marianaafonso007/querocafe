﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Entity
{
    public class CafeHistorico
    {
        public int ID { get; set; }

        public int CafeID { get; set; }

        public int UsuarioID { get; set; }

        public CafeStatus Status { get; set; }

        public DateTime DataCriacao { get; set; }

    }
}
