﻿using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Repository
{
    public class CafeRepository
    {
        private readonly QueroCafeDB conexaoDB;

        public CafeRepository(IConfiguration configuration)
        {
            conexaoDB = new QueroCafeDB(configuration);
        }

        public void Incluir(Cafe cafe)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO Cafe VALUES ({cafe.Agenda.ID}, {cafe.QtdXicaras}, {(int)cafe.Status}, GETDATE())";
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public Cafe BuscarPorStatus(CafeStatus status)
        {
            Cafe cafe = null;

            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"SELECT TOP 1 * FROM Cafe WHERE StatusID = {(int)status} ORDER BY DataCriacao DESC";
                
                var dataReader = cmd.ExecuteReader();

                if (dataReader.Read())
                {
                    cafe = MapCafe(dataReader);
                }
                
                cnn.Close();
            }

            return cafe;
        }

        internal void Atualizar(Cafe cafe)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"UPDATE Cafe
                                  SET QtdXicaras = {cafe.QtdXicaras},
                                  StatusID = {(int)cafe.Status}
                                  WHERE ID = {cafe.ID}";
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public Cafe BuscarPorID(int id)
        {
            Cafe cafe = null;

            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"SELECT * FROM Cafe WHERE ID = {id}";

                var dataReader = cmd.ExecuteReader();

                if (dataReader.Read())
                {
                    cafe = MapCafe(dataReader);
                }

                cnn.Close();
            }

            return cafe;
        }

        private Cafe MapCafe(SqlDataReader dataReader)
        {
            return new Cafe()
            {
                ID = Convert.ToInt32(dataReader["ID"]),
                DataCriacao = DateTime.Parse(dataReader["DataCriacao"].ToString()),
                QtdXicaras = Convert.ToInt32(dataReader["QtdXicaras"]),
                Status = Enum.Parse<CafeStatus>(dataReader["StatusID"].ToString()),
                Agenda = new Agenda()
                {
                    ID = Convert.ToInt32(dataReader["AgendamentoID"])
                }
            };
        }
    }
}
