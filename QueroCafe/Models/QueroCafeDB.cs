﻿using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace QueroCafe.Models
{
    public class QueroCafeDB : IQueroCafeDB
    {
        private IConfiguration _configuration;

        public QueroCafeDB(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }

        public SqlConnection Conexao()
        {
            var connectionString = _configuration.GetConnectionString("QueroCafe");
            SqlConnection sqlConexao = new SqlConnection(connectionString);
            sqlConexao.Open();
            return sqlConexao;
        }


    }
}
