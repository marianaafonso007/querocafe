﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Entity
{
    public class Checkin
    {
        public int ID { get; set; }

        public Agenda Agenda { get; set; }

        public DateTime DataCriacao { get; set; }
    }
}
