﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using QueroCafe.Models.Entity;

namespace QueroCafe.Models
{
    public static class Sessao
    {
        public static IHttpContextAccessor _httpContextAccessor;
        private static Usuario usuario;

        private static Usuario Usuario
        {
            get
            {
                if (usuario == null)
                    usuario = JsonConvert.DeserializeObject<Usuario>(_httpContextAccessor.HttpContext.Session.GetString("SessionUser"));

                return usuario;
            }
        }

        public static string NomeUsuario
        {
            get
            {
                return Usuario.Nome;
            }
        }

        public static int IdUsuario
        {
            get
            {
                return Usuario.Id;
            }
        }

        public static string EmailUsuario
        {
            get
            {
                return Usuario.Email;
            }
        }
    }
}
