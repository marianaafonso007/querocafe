﻿function validacao(frm) {
    if (frm.txtnome.value == "" || frm.txtnome.value == null) {
        alert("O nome do usuário deve ser informado!");
        frm.txtnome.focus();
        return false;
    }
    if (frm.txtemail.value == "" || frm.txtemail.value == null || frm.txtemail.value.indexOf("@") == -1 ||
        frm.txtemail.value.indexOf(".") == -1 ) {
        alert("O email do usuário deve ser informado!");
        frm.txtemail.focus();
        return false;
    }
    if (frm.txtcelular.value == "" || frm.txtcelular.value == null || frm.txtcelular.value.length != 11) {
        alert("Favor preencher o DD + Número do seu celular!");
        frm.txtcelular.focus();
        return false;
    }
    if (frm.codigoSituacao.value == 2) {
        alert("Favor selecionar a situação do usuário.");
        frm.codigoSituacao.focus();
        return false;
    }
}

function AceitaApenasNumero(e) {
    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58)) {
        return true;
    }
    else {
        if (tecla == 8 || tecla == 0) return true;
        else return false;
    }
}

function validacaCadastrarSenha(frm) {
    if (frm.txtsenhaNova.value == "" || frm.txtsenhaNova.value == null || frm.txtsenhaNova.value.length <= 5) {
        alert("Informe uma senha válida!");
        frm.txtsenha.focus();
        return false;
    }
    if (frm.txtsenhaConfirmacao.value == "" || frm.txtsenhaConfirmacao.value == null || frm.txtsenhaConfirmacao.value.length <= 5) {
        alert("Informe uma senha válida!");
        frm.txtsenha.focus();
        return false;
    }
    if (frm.txtsenhaNova.value != frm.txtsenhaConfirmacao.value) {
        alert("As senhas não correspondem, favor verifique o conteúdo digitado!");
        frm.txtsenha.focus();
        return false;
    }

}