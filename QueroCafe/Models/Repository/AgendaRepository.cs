﻿using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Repository
{
    public class AgendaRepository
    {
        private readonly QueroCafeDB conexaoDB;

        public AgendaRepository(IConfiguration configuration)
        {
            conexaoDB = new QueroCafeDB(configuration);
        }

        public List<Agenda> Buscar()
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @"SELECT a.*, u.nome
                                    FROM Agendamento a
                                    INNER JOIN Usuario u ON a.UsuarioID = u.id";

                var agendas = new List<Agenda>();
                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    agendas.Add(MapAgenda(dataReader));
                }

                cnn.Close();

                return agendas;
            }
        }

        public Agenda Buscar(int id)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"SELECT a.*, u.nome
                                     FROM Agendamento a
                                     INNER JOIN Usuario u ON a.UsuarioID = u.id
                                     WHERE a.ID = {id}";

                Agenda agenda = null;
                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    agenda = MapAgenda(dataReader);
                }

                cnn.Close();

                return agenda;
            }
        }

        public Agenda Buscar(int usuarioID, DateTime dataAgendamento)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"SELECT a.*, u.nome
                                     FROM Agendamento a
                                     INNER JOIN Usuario u ON a.UsuarioID = u.id
                                     WHERE a.UsuarioID = {usuarioID}
                                     AND DataAgendamento = '{dataAgendamento:yyyy-MM-dd}'";

                Agenda agenda = null;
                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    agenda = MapAgenda(dataReader);
                }

                cnn.Close();

                return agenda;
            }
        }

        public Agenda Buscar(DateTime dataAgendamento)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"SELECT a.*, u.nome
                                     FROM Agendamento a
                                     INNER JOIN Usuario u ON a.UsuarioID = u.id
                                     WHERE DataAgendamento = '{dataAgendamento:yyyy-MM-dd}'";

                Agenda agenda = null;
                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    agenda = MapAgenda(dataReader);
                }

                cnn.Close();

                return agenda;
            }
        }

        public void Alterar(Agenda agenda)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"UPDATE Agendamento
                                     SET DataAgendamento = '{agenda.DataAgendamento:yyyy-MM-dd}'
                                     WHERE ID = {agenda.ID}";

                cmd.ExecuteNonQuery();
            }
        }

        public void Incluir(Agenda agenda)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO Agendamento (UsuarioID, DataAgendamento, TipoAcao) VALUES ({agenda.Usuario.Id}, '{agenda.DataAgendamento:yyyy-MM-dd}', {(int)agenda.Acao})";
                    
                cmd.ExecuteNonQuery();

                cnn.Close();
            }
        }

        public void Deletar(int id)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"DELETE FROM Agendamento WHERE ID = {id}";

                cmd.ExecuteNonQuery();

                cnn.Close();
            }
        }

        private Agenda MapAgenda(SqlDataReader dataReader)
        {
            return new Agenda
            {
                ID = Convert.ToInt32(dataReader["Id"]),
                Usuario = new Usuario()
                {
                    Id = Convert.ToInt32(dataReader["UsuarioID"]),
                    Nome = dataReader["nome"].ToString()
                },
                DataAgendamento = DateTime.Parse(dataReader["DataAgendamento"].ToString()),
                Acao = Enum.Parse<TipoAcao>(dataReader["TipoAcao"].ToString())
            };
        }

    }
}
