﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QueroCafe.Filters;
using QueroCafe.Models;
using QueroCafe.Models.Entity;
using QueroCafe.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Controllers
{
    [Autenticacao]
    public class AgendaController : Controller
    {
        private readonly AgendaRepository agendaRepository;
        private readonly UsuarioRepository usuarioRepository;
        private readonly CheckinRepository checkinRepository;

        public AgendaController(IConfiguration configuration)
        {
            this.agendaRepository = new AgendaRepository(configuration);
            this.usuarioRepository = new UsuarioRepository(configuration);
            this.checkinRepository = new CheckinRepository(configuration);
        }

        public IActionResult Index()
        {
            var agendas = agendaRepository.Buscar();

            return View(agendas);
        }

        [HttpGet]
        public IActionResult Criar()
        {
            var usuarios = usuarioRepository.ListarUsuarios();

            ViewBag.Usuarios = usuarios;

            return View(new Agenda());
        }

        [HttpPost]
        public IActionResult Criar(string cbxUsuario, DateTime dtDataAgendada, TipoAcao cbxAcao)
        {
            ViewBag.Acao = "CRIAR";

            var agenda = new Agenda();
            var agendaAtual = agendaRepository.Buscar(dtDataAgendada);
            var checkin = checkinRepository.BuscarPorAgenda(agendaAtual?.ID ?? 0);

            if (agendaAtual != null && checkin == null)
            {
                ViewBag.Sucesso = false;
                ViewBag.Mensagem = $"Já existe uma data agendada nesse dia para o usuario {agendaAtual.Usuario.Nome}.";
            }
            else
            {
                agenda = new Agenda()
                {
                    Usuario = new Usuario { Id = Convert.ToInt32(cbxUsuario) },
                    DataAgendamento = dtDataAgendada,
                    Acao = cbxAcao
                };

                agendaRepository.Incluir(agenda);

                ViewBag.Sucesso = true;
            }

            ViewBag.Usuarios = usuarioRepository.ListarUsuarios();

            return View(agenda);
        }

        [HttpGet]
        public IActionResult Editar(int id)
        {
            var agenda = agendaRepository.Buscar(id);

            ViewBag.Usuarios = usuarioRepository.ListarUsuarios();

            return View("Criar", agenda ?? new Agenda());
        }

        [HttpPost]
        public IActionResult Editar(int id, string cbxUsuario, DateTime dtDataAgendada)
        {
            agendaRepository.Alterar(new Agenda()
            {
                ID = id,
                DataAgendamento = dtDataAgendada
            });

            ViewBag.Sucesso = true;
            ViewBag.Usuarios = usuarioRepository.ListarUsuarios();

            return View("Criar", agendaRepository.Buscar(id));
        }

        public IActionResult Deletar(int id)
        {
            ViewBag.Acao = "DELETE";

            try
            {
                agendaRepository.Deletar(id);

                ViewBag.Sucesso = true;
            }
            catch (Exception)
            {
                ViewBag.Sucesso = false;
            }

            return View("Index", agendaRepository.Buscar());
        }
    }
}
