﻿using Microsoft.AspNetCore.Mvc;
using QueroCafe.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using QueroCafe.Models;
using System.Text.RegularExpressions;
using QueroCafe.Models.Entity;
using QueroCafe.Filters;
using QueroCafe.Models.Service;

namespace QueroCafe.Controllers
{
    [Autenticacao]
    public class UsuarioController : Controller
    {
        private readonly IConfiguration _configuration;

        public UsuarioController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            var usuarioRepository = new UsuarioRepository(_configuration);
            var usuarios =  usuarioRepository.ListarUsuariosAtivosInativos();
            return View(usuarios);
        }

        public IActionResult Criar()
        {
            var usuario = new Usuario();
            return View(usuario);
        }

        public IActionResult Editar(int id)
        {
            var usuarioRepository = new UsuarioRepository(_configuration);
            var usuario = usuarioRepository.ListarUsuario(id);
            return View("Criar", usuario);
        }

        public IActionResult CriarUsuario(string txtnome, string txtemail, string txtcelular, int id, int codigoSituacao)
        {
            var usuario = new Usuario();
            if (txtnome != null && txtemail != null && txtcelular != null)
            {
                usuario.Nome = txtnome;
                usuario.Email = txtemail;
                usuario.Dd = txtcelular.Substring(0, 2);
                usuario.Celular = txtcelular.Substring(2, 9);
                if (codigoSituacao == 0)
                    usuario.Ativo = false;
                else
                    usuario.Ativo = true;
                var usuarioRepository = new UsuarioRepository(_configuration);
                if(id != 0 )
                {
                    usuario.Id = id;
                    if (usuarioRepository.AlterarUsuario(usuario))
                    {
                        ViewBag.Sucesso = true;
                    }
                    else
                        ViewBag.Sucesso = false;
                }
                else
                {
                    if (usuarioRepository.CriarUsuario(usuario))
                    {
                        var enviarEmail = new EmailService();
                        enviarEmail.EnvioEmail(usuario, "Novo usuário, SEJA BEM VINDO!");
                        ViewBag.Sucesso = true;
                        return View("Criar", new Usuario());
                    }
                    else
                        ViewBag.Sucesso = false;
                }
                
            }
            else
                ViewBag.Sucesso = false;

            return View("Criar", usuario);
        }
    }
}
