﻿using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Repository
{
    public class CheckinRepository
    {
        private readonly QueroCafeDB conexaoDB;

        public CheckinRepository(IConfiguration configuration)
        {
            conexaoDB = new QueroCafeDB(configuration);
        }

        public void Incluir(Checkin checkin)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO Checkin (AgendamentoID, DataCriacao) VALUES ({checkin.Agenda.ID}, GETDATE())";
                cmd.ExecuteNonQuery();
            }
        }

        public Checkin BuscarPorAgenda(int agendaID)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"SELECT *
                                     FROM Checkin
                                     WHERE AgendamentoID = {agendaID}";

                var dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    return new Checkin()
                    {
                        ID = Convert.ToInt32(dataReader["ID"]),
                        DataCriacao = DateTime.Parse(dataReader["DataCriacao"].ToString())
                    };
                }
            }

            return null;
        }
    }
}
