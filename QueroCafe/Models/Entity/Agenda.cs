﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Entity
{
    public class Agenda
    {
        public int ID { get; set; }

        public Usuario Usuario { get; set; }

        public DateTime DataAgendamento { get; set; }

        public TipoAcao Acao { get; set; }

        public Agenda()
        {
            Usuario = new Usuario();
        }
    }
}
