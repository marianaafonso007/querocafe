﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace QueroCafe.Filters
{
    public class AutenticacaoAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Session.GetString("SessionUser") == null)
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary(
                new
                {
                    area = "",
                    controller = "Login",
                    action = "Index"
                }));
            }
        }
    }
}
