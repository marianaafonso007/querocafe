﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Entity
{
    public enum TipoAcao
    {
        Comprar = 1,
        Preparar = 2
    }
}
