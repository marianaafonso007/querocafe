﻿using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Repository
{
    public class ControleAcessoRepository
    {
        private IQueroCafeDB conexaoDB;
        private IConfiguration _configuration;
        public ControleAcessoRepository(IConfiguration Configuration)
        {
            _configuration = Configuration;
            conexaoDB = new QueroCafeDB(_configuration);
        }

        public ControleAcesso ConsultaControleAcesso(int idUser)
        {
            var controleAcesso = new ControleAcesso();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM ControleAcesso " +
                    " WHERE UsuarioID='" + idUser + "'"
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    
                    controleAcesso.Id = Convert.ToInt32(dr["id"]);
                    controleAcesso.Usuario = new Usuario()
                    {
                        Id = Convert.ToInt32(dr["UsuarioID"])
                    };
                    controleAcesso.QuantidadeAcessoInvalido = int.Parse(dr["QuantidadeAcessoInvalido"].ToString());
                }
                return controleAcesso;
            }
            catch (Exception e)
            {
                return controleAcesso;
            }
        }

        public void Incluir(ControleAcesso controleAcesso)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO ControleAcesso (UsuarioID, QuantidadeAcessoInvalido, DataAcesso) VALUES ({controleAcesso.Usuario.Id}, '{controleAcesso.QuantidadeAcessoInvalido}', GETDATE())";

                cmd.ExecuteNonQuery();

                cnn.Close();
            }
        }

        public void Alterar(ControleAcesso controleAcesso)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = @$"UPDATE ControleAcesso
                                     SET QuantidadeAcessoInvalido = {controleAcesso.QuantidadeAcessoInvalido},
                                     DataAcesso = GETDATE()
                                     WHERE UsuarioID = {controleAcesso.Usuario.Id}";

                cmd.ExecuteNonQuery();
            }
        }

    }
}
