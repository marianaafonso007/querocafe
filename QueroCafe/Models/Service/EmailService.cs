﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
using QueroCafe.Models.Entity;

namespace QueroCafe.Models.Service
{
    public class EmailService
    {
        public bool EnvioEmail(Usuario usuario, string mensagem)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp-mail.outlook.com";
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("indtexbr_tcc@outlook.com", "X27wYgLi");
            MailMessage mail = new MailMessage();
            mail.Sender = new System.Net.Mail.MailAddress("indtexbr_tcc@outlook.com", "QUERO_CAFE");
            mail.From = new MailAddress("indtexbr_tcc@outlook.com", "CAFEZEIRO");
            mail.To.Add(new MailAddress(usuario.Email, "Tomador de café (:"));
            mail.Subject = "QUERO CAFÉ - AVISO";
            mail.Body = " Olá Cafezeiro, temos uma notícia:<br/>  : " + usuario.Nome + " <br/> Mensagem : " + mensagem;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            try
            {
                client.Send(mail);
                return true;
            }
            catch (System.Exception erro)
            {
                //trata erro
                return false;
            }
        }
    }
}
