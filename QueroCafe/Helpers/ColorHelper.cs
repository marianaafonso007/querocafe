﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Helpers
{
    public static class ColorHelper
    {
        private static string[] cores = new string[] 
        {
            "e57373", "F06292", "BA68C8", "9575CD", "7986CB", "64B5F6", "4FC3F7", "4DD0E1",
            "4DB6AC", "81C784", "AED581", "DCE775", "FFF176", "FFD54F", "FFB74D", "FF8A65",
            "A1887F", "E0E0E0", "90A4AE", "d32f2f", "C2185B", "7B1FA2", "512DA8", "303F9F",
            "1976D2", "0288D1"
        };

        public static string GetCorPorLetra(char letra)
        {
            var posicao = char.ToUpper(letra) - 64;

            return cores[posicao];
        }
    }
}
