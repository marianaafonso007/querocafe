﻿namespace QueroCafe.Models.Entity
{
    public enum CafeStatus
    {
        NaoDefinido,
        Comprado,
        Preparando,
        Pronto,
        Retirado,
        Esgotado
    }
}