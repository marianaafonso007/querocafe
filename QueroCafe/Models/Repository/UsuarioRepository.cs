﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using QueroCafe.Models;
using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;

namespace QueroCafe.Models.Repository
{
    public class UsuarioRepository
    {
        private IQueroCafeDB conexaoDB;
        private IConfiguration _configuration;
        public UsuarioRepository(IConfiguration Configuration)
        {
            _configuration = Configuration;
            conexaoDB = new QueroCafeDB(_configuration);
        }

        public Boolean CriarUsuario(Usuario usuario)
        {
            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO Usuario VALUES ('" + usuario.Nome + "', '" +
                    usuario.Email + "', '" + usuario.Dd + "', '" + usuario.Celular + "', '', GETDATE(), "+ Convert.ToByte(usuario.Ativo) + ", " + 1 + ")"
                    , conexaoDB.Conexao());
                command.ExecuteReader();
                conexaoDB.Conexao().Close();
                return true;                
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Boolean AlterarUsuario(Usuario usuario)
        {
            try
            {
                if(usuario.Senha != null)
                {
                    SqlCommand command = new SqlCommand("UPDATE Usuario SET " +
                   "nome= '" + usuario.Nome + "' ," + " dd='" + usuario.Dd + "' ," + " celular='" + usuario.Celular + "' ," +
                   " ativo=" + Convert.ToByte(usuario.Ativo) + ", senha='" + usuario.Senha + "', dataModificacao= GETDATE() WHERE id=" + usuario.Id 
                   , conexaoDB.Conexao());
                    command.ExecuteReader();
                    conexaoDB.Conexao().Close();
                }
                else
                {
                    SqlCommand command = new SqlCommand("UPDATE Usuario SET " +
                   "nome= '" + usuario.Nome + "' ," + " dd='" + usuario.Dd + "' ," + " celular='" + usuario.Celular + "' ,"
                   + " ativo="+ Convert.ToByte(usuario.Ativo) + ", dataModificacao= GETDATE() WHERE id=" + usuario.Id
                   , conexaoDB.Conexao());
                    command.ExecuteReader();
                    conexaoDB.Conexao().Close();
                }
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Usuario> ListarUsuarios()
        {
            var usuarios = new List<Usuario>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT id, nome, email, dd, celular FROM Usuario WHERE ativo = 1"
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while(dr.Read())
                {
                    var usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(dr["id"]);
                    usuario.Nome = dr["nome"].ToString();
                    usuario.Email = dr["email"].ToString();
                    usuario.Dd = dr["dd"].ToString();
                    usuario.Celular = dr["celular"].ToString();
                    usuarios.Add(usuario);
                }
                return usuarios;
            }
            catch (Exception e)
            {
                return usuarios;
            }
        }

        public List<Usuario> ListarUsuariosAtivosInativos()
        {
            var usuarios = new List<Usuario>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT id, nome, email, dd, celular FROM Usuario"
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    var usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(dr["id"]);
                    usuario.Nome = dr["nome"].ToString();
                    usuario.Email = dr["email"].ToString();
                    usuario.Dd = dr["dd"].ToString();
                    usuario.Celular = dr["celular"].ToString();
                    usuarios.Add(usuario);
                }
                return usuarios;
            }
            catch (Exception e)
            {
                return usuarios;
            }
        }

        public Usuario ListarUsuario(int id)
        {
            var usuario = new Usuario();
            try
            {
                SqlCommand command = new SqlCommand("SELECT id, nome, email, dd, celular FROM Usuario WHERE id = " + id.ToString()
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    usuario.Id = Convert.ToInt32(dr["id"]);
                    usuario.Nome = dr["nome"].ToString();
                    usuario.Email = dr["email"].ToString();
                    usuario.Dd = dr["dd"].ToString();
                    usuario.Celular = dr["celular"].ToString();
                }
                return usuario;
            }
            catch (Exception e)
            {
                return usuario;
            }
        }

        public Usuario ConsultarUsuario(string email, string senha)
        {
            var usuario = new Usuario();
            try
            {
                SqlCommand command = new SqlCommand("SELECT id, nome, email, dd, celular, ativo, bloqueado FROM Usuario WHERE email = '" + email + "' AND senha ='" + senha + "'"
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    usuario.Id = Convert.ToInt32(dr["id"]);
                    usuario.Nome = dr["nome"].ToString();
                    usuario.Email = dr["email"].ToString();
                    usuario.Dd = dr["dd"].ToString();
                    usuario.Ativo = bool.Parse(dr["ativo"].ToString());
                    usuario.Bloqueado = bool.Parse(dr["bloqueado"].ToString());
                }
                return usuario;
            }
            catch (Exception e)
            {
                return usuario;
            }
        }

        public Usuario VerificaEmail(string email)
        {
            var usuario = new Usuario();
            try
            {
                SqlCommand command = new SqlCommand("SELECT id, email FROM Usuario WHERE email = '" + email + "'"
                    , conexaoDB.Conexao());
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    usuario.Id = Convert.ToInt32(dr["id"]);
                    usuario.Email = dr["email"].ToString();
                }
                return usuario;
            }
            catch (Exception e)
            {
                return usuario;
            }
        }

        public bool BloqueioUsuario(string email)
        {
            var usuario = new Usuario();
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Usuario SET bloqueado = 1 WHERE email='" + email  + "'"
                    , conexaoDB.Conexao());
                command.ExecuteReader();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
