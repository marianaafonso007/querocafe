﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QueroCafe.Models;
using QueroCafe.Models.Entity;
using QueroCafe.Models.Repository;
using System.Collections.Generic;
using System.Linq;

namespace QueroCafe.Controllers
{
    public class LoginController : Controller
    {
        private readonly IConfiguration _configuration;
        private int _tentativaAcesso;

        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Entrar(string txtemail, string txtsenha)
        {
            var usuarioRepository = new UsuarioRepository(_configuration);
            var usuario = usuarioRepository.ConsultarUsuario(txtemail, txtsenha);

            if(usuario.Email == txtemail)
            {
                if(usuario.Bloqueado != true && usuario.Ativo != false)
                {
                    var controleAcesso = BuscaControleAcesso(usuario.Id);
                    if (controleAcesso.Id == 0)
                    {
                        _tentativaAcesso = 1;
                        IncluirControleAcesso(usuario, _tentativaAcesso);
                    }
                    else
                    {
                        _tentativaAcesso = 1;
                        AlteraControleAcesso(usuario, _tentativaAcesso);
                    }
                    HttpContext.Session.SetString("SessionUser", JsonConvert.SerializeObject(usuario));
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    var login = new Login();
                    login.Id = 1;
                    login.Ativo = usuario.Ativo;
                    login.Bloqueado = usuario.Bloqueado;
                    ViewBag.Sucesso = false;
                    return View("Index", login);
                }
            }
            else
            {
                var login = new Login();
                var user = ValidaEmailValido(txtemail);
                if(user.Id != 0)
                {
                    ValidaTentativa(user);
                    if (BloqueiaUsuario(user.Email))
                        login.Bloqueado = true;
                    else
                        login.Bloqueado = false;
                }
                ViewBag.Sucesso = false;
                return View("Index", login);
            }
        }
        public IActionResult CadastrarSenha()
        {
            return View();
        }

        public IActionResult Cadastrar(string txtsenhaNova)
        {
            return View();
        }
        public Usuario ValidaEmailValido(string txtemail)
        {
            var usuario = new Usuario();
            var usuarioRepository = new UsuarioRepository(_configuration);
            usuario = usuarioRepository.VerificaEmail(txtemail);
            return usuario;
        }
        public void ValidaTentativa(Usuario user)
        {
            var controleAcesso = BuscaControleAcesso(user.Id);
            if(controleAcesso.Id == 0)
            {
                _tentativaAcesso = 1;
                IncluirControleAcesso(user, _tentativaAcesso);
            }
            else
            {
                _tentativaAcesso = controleAcesso.QuantidadeAcessoInvalido;
                var quantidadeAlterar = _tentativaAcesso + 1;
                AlteraControleAcesso(user, quantidadeAlterar);
            }
        }

        public bool BloqueiaUsuario(string txtemail)
        {
            if(_tentativaAcesso > 5)
            {
                var usuarioRepository = new UsuarioRepository(_configuration);
                return usuarioRepository.BloqueioUsuario(txtemail);
            }
            return false;
        }
        public ControleAcesso BuscaControleAcesso(int idUser)
        {
            var controleAcesso = new ControleAcesso();
            var controleAcessoRepository = new ControleAcessoRepository(_configuration);
            controleAcesso = controleAcessoRepository.ConsultaControleAcesso(idUser);
            return controleAcesso;
        }
        public void IncluirControleAcesso(Usuario user, int tentativa)
        {
            var controleAcesso = new ControleAcesso();
            var controleAcessoRepository = new ControleAcessoRepository(_configuration);
            controleAcesso.Usuario = user;
            controleAcesso.QuantidadeAcessoInvalido = tentativa;
            controleAcessoRepository.Incluir(controleAcesso);
        }
        public void AlteraControleAcesso(Usuario user, int tentativa)
        {
            var controleAcesso = new ControleAcesso();
            var controleAcessoRepository = new ControleAcessoRepository(_configuration);
            controleAcesso.Usuario = user;
            controleAcesso.QuantidadeAcessoInvalido = tentativa;
            controleAcessoRepository.Alterar(controleAcesso);
        }

        public IActionResult Sair()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Index");
        }
    }
}
