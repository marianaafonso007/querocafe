﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using QueroCafe.Filters;
using QueroCafe.Models.Entity;
using QueroCafe.Models.Repository;

namespace QueroCafe.Controllers
{
    [Autenticacao]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
