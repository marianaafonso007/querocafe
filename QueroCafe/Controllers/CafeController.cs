﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using QueroCafe.Models;
using QueroCafe.Models.Entity;
using QueroCafe.Models.Repository;
using QueroCafe.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Controllers
{
    public class CafeController : Controller
    {
        private readonly CafeRepository cafeRepository;
        private readonly CafeHistoricoRepository cafeHistoricoRepository;
        private readonly UsuarioRepository usuarioRepository;

        public CafeController(IConfiguration configuration)
        {
            cafeRepository = new CafeRepository(configuration);
            cafeHistoricoRepository = new CafeHistoricoRepository(configuration);
            usuarioRepository = new UsuarioRepository(configuration);
        }

        public IActionResult Index()
        {
            var cafe = cafeRepository.BuscarPorStatus(CafeStatus.Pronto);

            if (cafe == null)
                cafe = cafeRepository.BuscarPorStatus(CafeStatus.Comprado);

            return View(cafe);
        }

        public IActionResult Preparar(int cafeID)
        {
            var cafe = cafeRepository.BuscarPorID(cafeID);

            cafe.Status = CafeStatus.Preparando;

            cafeRepository.Atualizar(cafe);
            cafeHistoricoRepository.Incluir(new CafeHistorico()
            {
                CafeID = cafeID,
                UsuarioID = Sessao.IdUsuario,
                Status = CafeStatus.Preparando
            });

            return View("Index", cafe);
        }

        public IActionResult Pronto(int cafeID)
        {
            var cafe = cafeRepository.BuscarPorID(cafeID);
            var usuarios = usuarioRepository.ListarUsuarios();
            var email = new EmailService();

            cafe.Status = CafeStatus.Pronto;

            cafeRepository.Atualizar(cafe);
            cafeHistoricoRepository.Incluir(new CafeHistorico()
            {
                CafeID = cafeID,
                UsuarioID = Sessao.IdUsuario,
                Status = CafeStatus.Pronto
            });

            foreach (var usuario in usuarios)
            {
                email.EnvioEmail(usuario, $"{Sessao.NomeUsuario} acabou de fazer um cafézinho. Eai bora pegar um pouco? &#128525;");
            }

            return View("Index", cafe);
        }

        public IActionResult Retirar(int cafeID)
        {
            var cafe = cafeRepository.BuscarPorID(cafeID);
            var usuarios = usuarioRepository.ListarUsuarios();
            var email = new EmailService();

            if (cafe.QtdXicaras > 0)
                cafe.QtdXicaras--;
            else
                throw new Exception("Não há mais café para retirar.");

            if (cafe.QtdXicaras == 0)
                cafe.Status = CafeStatus.Esgotado;

            cafeRepository.Atualizar(cafe);
            cafeHistoricoRepository.Incluir(new CafeHistorico()
            {
                CafeID = cafeID,
                UsuarioID = Sessao.IdUsuario,
                Status = CafeStatus.Retirado
            });

            if (cafe.Status == CafeStatus.Esgotado)
            {
                cafeHistoricoRepository.Incluir(new CafeHistorico()
                {
                    CafeID = cafeID,
                    UsuarioID = Sessao.IdUsuario,
                    Status = CafeStatus.Esgotado
                });

                foreach (var usuario in usuarios)
                {
                    email.EnvioEmail(usuario, $"{Sessao.NomeUsuario} pegou a ultima xícara de café &#128546;. Não tem mais café, caso queira pegar um pouco.");
                }
            }

            return View("Index", cafe);
        }
    }
}
