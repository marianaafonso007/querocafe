﻿using Microsoft.Extensions.Configuration;
using QueroCafe.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Repository
{
    public class CafeHistoricoRepository
    {
        private readonly QueroCafeDB conexaoDB;

        public CafeHistoricoRepository(IConfiguration configuration)
        {
            conexaoDB = new QueroCafeDB(configuration);
        }

        public void Incluir(CafeHistorico cafeHistorico)
        {
            using (var cnn = conexaoDB.Conexao())
            using (var cmd = cnn.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO CafeHistorico VALUES ({cafeHistorico.CafeID}, {cafeHistorico.UsuarioID}, {(int)cafeHistorico.Status}, GETDATE())";
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }
    }
}
