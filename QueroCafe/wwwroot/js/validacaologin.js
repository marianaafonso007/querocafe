﻿function validacao(frm) {
    if (frm.txtemail.value == "" || frm.txtemail.value == null || frm.txtemail.value.indexOf("@") == -1 ||
        frm.txtemail.value.indexOf(".") == -1 ) {
        alert("O email do usuário deve ser informado!");
        frm.txtemail.focus();
        return false;
    }
    if (frm.txtsenha.value == "" || frm.txtsenha.value == null || frm.txtsenha.value.length <= 5) {
        alert("Sua senha deve ser informada");
        frm.txtsenha.focus();
        return false;
    }
}
