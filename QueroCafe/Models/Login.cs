﻿namespace QueroCafe.Models
{
    public class Login
    {
        public int Id { get; set; }
        public bool Ativo { get; set; }
        public bool Bloqueado { get; set; }
    }
}
