﻿using System.Data.SqlClient;

namespace QueroCafe.Models
{
    public interface IQueroCafeDB
    {
        SqlConnection Conexao();
    }
}