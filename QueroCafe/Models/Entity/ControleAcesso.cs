﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueroCafe.Models.Entity
{
    public class ControleAcesso
    {
        public int Id { get; set; }
        public Usuario Usuario { get; set; }
        public int QuantidadeAcessoInvalido { get; set; }
        public DateTime dataModificacao { get; set; }

    }
}
